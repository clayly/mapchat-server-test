package com.example.demo;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

@Component
public class ThisRestHandler {
    public Mono<ServerResponse> getFeature(ServerRequest request) {
        return ServerResponse
                .ok()
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(GeoJson.rngFeature())
                .log();
    }

    public Mono<ServerResponse> getFeatureCollection(ServerRequest request) {
        return ServerResponse
                .ok()
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(GeoJson.rngFeatureCollection(1000))
                .log();
    }
}

