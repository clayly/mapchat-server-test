package com.example.demo;

import com.vdurmont.emoji.Emoji;
import com.vdurmont.emoji.EmojiManager;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public final class GeoJson {

    private GeoJson() {
        throw new AssertionError();
    }

    @Data
    @AllArgsConstructor
    public static class FeatureCollection {
        @NonNull
        public final String type = "FeatureCollection";

        @NonNull
        public List<Feature> features;
    }


    @Data
    @AllArgsConstructor
    public static class Feature {
        @NonNull
        public final String type = "Feature";

        @NonNull
        public Point geometry;
        @NonNull
        public Properties properties;
    }


    @AllArgsConstructor
    @Data
    public static class Point {
        @NonNull
        public final String type = "Point";

        @NonNull
        public Double[] coordinates;
    }

    @Data
    @AllArgsConstructor
    public static class Properties {
        public String name;
    }

    public static <T> T rngChoose(List<T> objects) {
        final var rng = ThreadLocalRandom.current();
        return objects.get(rng.nextInt(0, objects.size()));
    }

    public static String rngString(int length) {
        final var rng = ThreadLocalRandom.current();
        final var chars = new byte[length];
        rng.nextBytes(chars);
        return new String(chars, StandardCharsets.UTF_8);
    }

    public static final Collection<Emoji> EMOJI_COLLECTION = EmojiManager.getAll();
    public static final Emoji[] EMOJI_ARR = EMOJI_COLLECTION.toArray(new Emoji[0]);

    public static String rngEmoji() {
        final var rng = ThreadLocalRandom.current();
        final var emoji = EMOJI_ARR[rng.nextInt(0, EMOJI_ARR.length)];
        return emoji.getUnicode();
    }

    public static Feature createFeature(Double lat, Double lng, String name) {
        return new Feature(
                new Point(new Double[]{lng, lat}),
                new Properties(name)
        );
    }

    public static Feature rngFeature() {
        final var rng = ThreadLocalRandom.current();
        return createFeature(
                rng.nextDouble(-90, 90),
                rng.nextDouble(-180, 180),
//                rngString(1)
//                rngChoose(List.of("😀", "❤"))
//                rngChoose(List.of("\\uD83D\\uDE00", "\\uD83D\\uDC96"))
//                rngChoose(List.of("💖", "💀"))
                rngEmoji()
        );
    }

    public static FeatureCollection rngFeatureCollection(int count) {
        return new FeatureCollection(Stream
                .generate(GeoJson::rngFeature)
                .limit(count)
                .collect(Collectors.toUnmodifiableList())
        );
    }

}

