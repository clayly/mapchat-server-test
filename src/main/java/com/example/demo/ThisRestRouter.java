package com.example.demo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

@Configuration
public class ThisRestRouter {

    @Bean
    public RouterFunction<ServerResponse> route(ThisRestHandler handler) {
        return RouterFunctions
                .route(RequestPredicates.GET("/feature").and(RequestPredicates.accept(MediaType.TEXT_PLAIN)), handler::getFeature)
                .andRoute(RequestPredicates.GET("/featureCollection").and(RequestPredicates.accept(MediaType.TEXT_PLAIN)), handler::getFeatureCollection);

    }
}